﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PreSellBookingWebApi.Migrations
{
    public partial class ChangeDataMode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Leader_Email",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Leader_FirstName",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Leader_LastName",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Leader_PhoneNumber",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "Leader_Gender",
                table: "Users",
                newName: "Gender");

            migrationBuilder.AlterColumn<int>(
                name: "Gender",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "UserDefaultCompanions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    CompanionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDefaultCompanions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDefaultCompanions_Companions_CompanionId",
                        column: x => x.CompanionId,
                        principalTable: "Companions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_UserDefaultCompanions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDefaultCompanions_CompanionId",
                table: "UserDefaultCompanions",
                column: "CompanionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDefaultCompanions_UserId",
                table: "UserDefaultCompanions",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDefaultCompanions");

            migrationBuilder.RenameColumn(
                name: "Gender",
                table: "Users",
                newName: "Leader_Gender");

            migrationBuilder.AlterColumn<int>(
                name: "Leader_Gender",
                table: "Users",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Leader_Email",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Leader_FirstName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Leader_LastName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Leader_PhoneNumber",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Hotels",
                type: "ntext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }
    }
}
