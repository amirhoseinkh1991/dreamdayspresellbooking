﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PreSellBookingWebApi.Migrations
{
    public partial class MakeCompanionReservationIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companions_Reservations_ReservationId",
                table: "Companions");

            migrationBuilder.AlterColumn<int>(
                name: "ReservationId",
                table: "Companions",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Companions_Reservations_ReservationId",
                table: "Companions",
                column: "ReservationId",
                principalTable: "Reservations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companions_Reservations_ReservationId",
                table: "Companions");

            migrationBuilder.AlterColumn<int>(
                name: "ReservationId",
                table: "Companions",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Companions_Reservations_ReservationId",
                table: "Companions",
                column: "ReservationId",
                principalTable: "Reservations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
