﻿using Microsoft.AspNetCore.Mvc;
using PreSellBookingWebApi.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Extensions
{
    public static class ControllerExtensionMethods
    {
        public static string GetCurrentUsername_JWT(this Controller self)
        {
            return self.User.FindFirst(JwtRegisteredClaimNames.Sub)?.Value;
        }

        public static User GetCurrentUser(this Controller self, DatabaseContext db)
        {
            return db.Users.Where(u => u.UserName == GetCurrentUsername_JWT(self)).FirstOrDefault();
        }

        public static int? GetCurrentUserId(this Controller self, DatabaseContext db)
        {
            var user = GetCurrentUser(self, db);
            if (user == null)
                return null;
            return user.Id;
        }
    }
}
