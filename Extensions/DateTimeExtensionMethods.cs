﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Extensions
{
    public static class DateTimeExtensionMethods
    {
        public static bool IsWeekend(this DateTime self)
        {
            return self.DayOfWeek == DayOfWeek.Wednesday || self.DayOfWeek == DayOfWeek.Thursday || self.DayOfWeek == DayOfWeek.Friday;
        }
    }
}
