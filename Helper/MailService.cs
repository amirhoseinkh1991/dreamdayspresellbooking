﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Helper
{
    public class MailService
    {
        private SmtpClient smtpClient;

        public MailService(SmtpClient _smtpClient)
        {
            smtpClient = _smtpClient;
        }

        public async Task SendMail(MailRequest mailRequest)
        {
            var mailMessage = new MailMessage(
                mailRequest.From,
                mailRequest.To,
                mailRequest.Subject,
                mailRequest.Body)
            {
                IsBodyHtml = mailRequest.IsBodyHtml
            };
            try
            {
                await smtpClient.SendMailAsync(mailMessage);
            } catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

    }

    public class MailRequest
    {
        public MailRequest()
        {
        }

        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
    }
}
