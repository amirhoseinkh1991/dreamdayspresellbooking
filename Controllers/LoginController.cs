﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PreSellBookingWebApi.DTO;
using PreSellBookingWebApi.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Controllers
{
    [Route("api/Login")]
    [ApiController]
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly IConfiguration config;
        private readonly DatabaseContext db;
        private readonly UserManager<User> userManager;

        public LoginController(DatabaseContext db, UserManager<User> userManager, IConfiguration config)
        {
            this.db = db;
            this.userManager = userManager;
            this.config = config;
        }

        [HttpPost]
        public async Task<LoginResponseDTO> Login([FromBody] LoginRequestDTO loginDto)
        {
            var user = await AuthenticateUser(loginDto);
            var tokenString = GenerateJSONWebToken(user);
            return new LoginResponseDTO
            {
                ExpirationDate = user.ExpirationDate,
                VoucherCode = user.UserName,
                Email = user.Email,
                LeaderFirstName = user.FirstName,
                Gender = user.Gender.ToString(),
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                Token = tokenString
            };
        }

        private string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new HashSet<Claim>() {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email)
            };

            foreach (var role in userManager.GetRolesAsync(user).Result)
                claims.Add(new Claim(ClaimTypes.Role, role));

            var token = new JwtSecurityToken(config["Jwt:Issuer"],
              config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<User> AuthenticateUser(LoginRequestDTO login)
        {
            var user = await db.Users.Where(u => u.UserName == login.UserName).FirstOrDefaultAsync();

            if (user == null)
                throw new Exception("Wrong VoucherCode Or Password.");

            var isValid = await userManager.CheckPasswordAsync(user, login.Password);
            if (!isValid)
                throw new Exception("Wrong VoucherCode Or Password.");
            if (user.ExpirationDate < DateTime.Today)
                throw new Exception("VoucherCode is Expired.");
            return user;
        }
    }
}
