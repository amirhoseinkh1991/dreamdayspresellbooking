﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PreSellBookingWebApi.DTO;
using PreSellBookingWebApi.Entities;
using PreSellBookingWebApi.Extensions;
using PreSellBookingWebApi.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Controllers
{
    [Authorize]
    [Route("api/User")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly DatabaseContext db;
        private readonly MailService mailService;
        private readonly UserManager<User> userManager;
        
        public UserController(DatabaseContext db, UserManager<User> userManager, MailService mailService)
        {
            this.db = db;
            this.userManager = userManager;
            this.mailService = mailService;
        }

        [HttpPost("Current/EditDetails")]
        public UserDTO EditUserDetails(EditUserDTO request)
        {
            var currentUser = db.Users.Include(u => u.DefaultCompanions).FirstOrDefault(u => u.UserName == this.GetCurrentUsername_JWT());
            currentUser.Email = request.Email ?? currentUser.Email;
            currentUser.FirstName = request.FirstName ?? currentUser.FirstName;
            currentUser.LastName = request.LastName ?? currentUser.LastName;
            currentUser.Gender = request.Gender == "Male" ? Gender.Male : request.Gender == "Female" ? Gender.Female : currentUser.Gender;
            currentUser.PhoneNumber = request.PhoneNumber ?? currentUser.PhoneNumber;
            currentUser.DefaultCompanions.Clear();
            currentUser.DefaultCompanions = request.DefaultCompanions
                .Select(c => new UserDefaultCompanion
                {
                    Companion = new Companion
                    {
                        CompanionType = c.CompanionType == "Adult" ? CompanionType.Adult : CompanionType.Child,
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        Gender = c.Gender == "Male" ? Gender.Male : c.Gender == "Female" ? Gender.Female : Gender.Unknown
                    },
                    UserId = currentUser.Id
                }).ToHashSet();
            db.SaveChanges();

            return new UserDTO
            {
                Id = currentUser.Id,
                Email = currentUser.Email,
                FirstName = currentUser.FirstName,
                Gender = currentUser.Gender.ToString(),
                LastName = currentUser.LastName,
                PhoneNumber = currentUser.PhoneNumber,
                VoucherCode = currentUser.UserName,
                ExpirationDate = currentUser.ExpirationDate,
                DefaultCompanions = currentUser.DefaultCompanions.Select(c => new CompanionDTO
                {
                    CompanionType = c.Companion.CompanionType.ToString(),
                    FirstName = c.Companion.FirstName,
                    LastName = c.Companion.LastName,
                    Gender = c.Companion.Gender.ToString()
                }).ToHashSet()
            };
        }

        [HttpGet("Current/Config")]
        public UserConfigDTO GetCurrentUserConfig()
        {
            var user = db.Users.Include("DefaultCompanions.Companion").Where(u => u.Id == this.GetCurrentUserId(db)).FirstOrDefault();
            var totalReservesRemaining = 6 - db.Reservations.Count(r => r.UserId == user.Id);
            var totalWeekendReservesRemaining = 4 - db.Reservations.Where(r => r.UserId == user.Id).ToList().Where(r => r.IsWeekend).Count();
            var defaultCompanions = user.DefaultCompanions.Select(c => new CompanionDTO
            {
                CompanionType = c.Companion.CompanionType.ToString(),
                FirstName = c.Companion.FirstName,
                LastName = c.Companion.LastName,
                Gender = c.Companion.Gender.ToString()
            }).ToHashSet();
            var childrenCompanionsCount = defaultCompanions.Count(c => c.CompanionType == CompanionType.Child.ToString());
            return new UserConfigDTO
            {
                Leader = new UserDTO
                {
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Gender = user.Gender.ToString(),
                    Id = user.Id,
                    PhoneNumber = user.PhoneNumber,
                    VoucherCode = user.UserName,
                    ExpirationDate = user.ExpirationDate,
                    Plan = user.UserPlan.ToString(),
                    DefaultCompanions = user.DefaultCompanions.Select(c => new CompanionDTO
                    {
                        CompanionType = c.Companion.CompanionType.ToString(),
                        FirstName = c.Companion.FirstName,
                        Gender = c.Companion.Gender.ToString(),
                        LastName = c.Companion.LastName
                    }).ToHashSet()
                },
                ChildrenCompanionsCount = childrenCompanionsCount,
                Companions = defaultCompanions,
                TotalReservesRemaining = totalReservesRemaining,
                TotalWeekendReservesRemaining = totalWeekendReservesRemaining
            };
        }

        [HttpGet("AddExampleData")]
        public async Task AddTestUsers()
        {
            var users = new HashSet<User>()
            {
                new User
                {
                    UserName = "1",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "2",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "3",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "4",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "5",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "6",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "7",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "8",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "9",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                },
                new User
                {
                    UserName = "10",
                    UserPlan = Plan.DreamStay,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                }
            };
            foreach (var user in users)
            {
                var _user = await userManager.FindByNameAsync(user.UserName);
                if (_user == null)
                {
                    var createUser = await userManager.CreateAsync(user, $"testuser{user.UserName}");
                    if (createUser.Succeeded)
                        await userManager.AddToRoleAsync(user, "User");
                }
            }
        }

        [AllowAnonymous]
        [HttpGet("email-test")]
        public async Task SendTestEmail()
        {
            var mail = new MailRequest
            {
                Body = "<h1>TestContent</h1>",
                From = "reservations@ddholidays.com",
                Subject = "Test Subject",
                To = "amir.hossein.khalouei@gmail.com",
                IsBodyHtml = true
            };
            await mailService.SendMail(mail);
        }
    }
}
