﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using PreSellBookingWebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Controllers
{
    [Authorize]
    [Route("api/Image")]
    [ApiController]
    public class ImageController : Controller
    {
        private readonly DatabaseContext db;

        public ImageController(DatabaseContext db)
        {
            this.db = db;
        }

        [AllowAnonymous]
        [HttpGet("{imageId}/View")]
        public IActionResult DownloadImage([FromRoute] int imageId)
        {
            var image = db.HotelImages.SingleOrDefault(img => img.Id == imageId);
            if (image == null)
                return NotFound();
            var file = System.IO.File.OpenRead($"Images/{image.FileName}");

            ContentDisposition cd = new ContentDisposition
            {
                FileName = image.FileName,
                Inline = true
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            Response.Headers.Add("X-Content-Type-Options", "nosniff");

            return File(file, "image/jpg");
        }
    }
}
