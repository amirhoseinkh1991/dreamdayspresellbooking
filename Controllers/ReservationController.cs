﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PreSellBookingWebApi.DTO;
using PreSellBookingWebApi.Entities;
using PreSellBookingWebApi.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Controllers
{
    [Authorize]
    [Route("api/Reservation")]
    [ApiController]
    public class ReservationController : Controller
    {
        private readonly DatabaseContext db;

        public ReservationController(DatabaseContext db)
        {
            this.db = db;
        }

        [HttpPost("Add")]
        public AddReservationResponseDTO AddNewReservation([FromBody] AddReservationRequestDTO request)
        {
            var user = db.Users.Include("DefaultCompanions.Companion").FirstOrDefault(u => u.Id == this.GetCurrentUserId(db));

            var reservation = new Reservation
            {
                BookingDate = DateTime.Today,
                CheckInDate = request.CheckInDate,
                CheckOutDate = request.CheckInDate.AddDays(2),
                HotelId = request.HotelId,
                UserId = user.Id,
                Companions = user.DefaultCompanions.Select(c => new Companion
                {
                    CompanionType = c.Companion.CompanionType,
                    FirstName = c.Companion.FirstName,
                    LastName = c.Companion.LastName,
                    Gender = c.Companion.Gender
                }).ToList()
            };
            db.Reservations.Add(reservation);
            db.SaveChanges();
            var result = db.Reservations.Include(r => r.Hotel).Include(r => r.User).Include(r => r.Companions)
                .Where(r => r.Id == reservation.Id).Select(r => new AddReservationResponseDTO
                {
                    BookingDate = r.BookingDate,
                    CheckInDate = r.CheckInDate,
                    CheckOutDate = r.CheckOutDate,
                    Companions = r.Companions.Select(c => new CompanionDTO
                    {
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        Gender = c.Gender.ToString(),
                        CompanionType = c.CompanionType.ToString()
                    }).ToList(),
                    ChildrenCount = r.Companions.Count(com => com.CompanionType == CompanionType.Child),
                    Hotel = new HotelDTO
                    {
                        Description = r.Hotel.Description,
                        Email = r.Hotel.Email,
                        HotelPlan = r.Hotel.HotelPlan,
                        Id = r.HotelId,
                        MealType = r.Hotel.MealType,
                        Name = r.Hotel.Name,
                        RoomType = r.Hotel.RoomType,
                        Stars = r.Hotel.Stars
                    },
                    Id = r.Id,
                    User = new UserDTO
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        VoucherCode = user.UserName,
                        Gender = user.Gender.ToString(),
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,
                        ExpirationDate = user.ExpirationDate
                    }
                }).FirstOrDefault();

            return result;
        }

        [HttpGet("List/ByCurrentUser")]
        public ICollection<AddReservationResponseDTO> GetCurrentUserReservations()
        {
            var user = this.GetCurrentUser(db);
            return db.Reservations.Include(r => r.Companions).Include(r => r.Hotel).AsNoTracking()
                .Where(r => r.UserId == user.Id).Select(r => new AddReservationResponseDTO
                {
                    BookingDate = r.BookingDate,
                    CheckInDate = r.CheckInDate,
                    CheckOutDate = r.CheckOutDate,
                    Companions = r.Companions.Select(c => new CompanionDTO
                    {
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        Gender = c.Gender.ToString(),
                        CompanionType = c.CompanionType.ToString()
                    }).ToList(),
                    ChildrenCount = r.Companions.Count(com => com.CompanionType == CompanionType.Child),
                    Hotel = new HotelDTO
                    {
                        Description = r.Hotel.Description,
                        Email = r.Hotel.Email,
                        HotelPlan = r.Hotel.HotelPlan,
                        Id = r.HotelId,
                        MealType = r.Hotel.MealType,
                        Name = r.Hotel.Name,
                        RoomType = r.Hotel.RoomType,
                        Stars = r.Hotel.Stars
                    },
                    Id = r.Id,
                    User = new UserDTO
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        VoucherCode = user.UserName,
                        Gender = user.Gender.ToString(),
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,
                        ExpirationDate = user.ExpirationDate
                    }
                }).ToList();

        }
    }
}
