﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PreSellBookingWebApi.DTO;
using PreSellBookingWebApi.Entities;
using PreSellBookingWebApi.Extensions;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Implementation.XmlSerialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Controllers
{
    [Authorize]
    [Route("api/Hotels")]
    [ApiController]
    public class HotelController : Controller
    {
        private readonly DatabaseContext db;
        public HotelController(DatabaseContext db)
        {
            this.db = db;
        }

        [HttpGet("Search")]
        public ICollection<HotelSearchResponseDTO> GetHotelsBuCurrentLoginUser([FromQuery] DateTime? checkInDate)
        {
            var user = this.GetCurrentUser(db);
            var totalReservesRemaining = 6 - db.Reservations.Count(r => r.UserId == user.Id);
            if (totalReservesRemaining == 0)
                return new HashSet<HotelSearchResponseDTO>();

            checkInDate ??= DateTime.Today;
            var checkOutDate = checkInDate.GetValueOrDefault().AddDays(2);

            var totalWeekendReservesRemaining = 4 - db.Reservations.Where(r => r.UserId == user.Id).ToList().Where(r => r.IsWeekend).Count();
            if (totalWeekendReservesRemaining == 0 && (checkInDate.Value.IsWeekend() || checkOutDate.AddDays(-1).IsWeekend()))
                return new HashSet<HotelSearchResponseDTO>();

            var children = db.UserDefaultCompanions.Include(udc => udc.Companion).Where(udc => udc.UserId == user.Id && udc.Companion.CompanionType == CompanionType.Child).Count();

            var userReservedHotelsIds = db.Reservations.Include(r => r.Hotel).Where(r => r.UserId == user.Id).Select(r => r.Hotel.Id).ToList();
            var userReserves = db.Reservations.Where(r => r.UserId == user.Id).ToList();
            var reservedHotelContDic = db.Reservations.Include(r => r.Hotel).Where(r => r.UserId == user.Id).GroupBy(r => r.HotelId).Select(g => new { g.Key, Count = g.Count() }).ToDictionary(x => x.Key, x => x.Count);

            var mainQuery = db.Hotels.Include(h => h.HotelChain).Include(h => h.AvailableDateIntervals).Include(h => h.Images).Include(h => h.StopSales)
                .Where(h => h.IsActive &&
                h.HotelPlan == user.UserPlan &&
                h.ChildrenCountLimit >= children &&
                h.AvailableDateIntervals.Any(interval => interval.StartDate <= checkInDate && interval.EndDate >= checkOutDate) &&
                (!h.SingleBookLimitPerUser) || (h.SingleBookLimitPerUser && !userReservedHotelsIds.Contains(h.Id)) &&
                (!h.HotelChainId.HasValue) || (h.HotelChainId.HasValue && (!h.SingleBookLimitPerUserByHotelChain || (h.SingleBookLimitPerUserByHotelChain && !userReservedHotelsIds.Any(urhid => urhid == h.HotelChainId.Value))))
                );

            var hotels = mainQuery
                .ToList()
                .Where(h => (!h.StopSales.Any(ss => ss.StartDate <= checkInDate || ss.EndDate >= checkOutDate.AddDays(-1))) &&
                            ((!h.OnlyWeekendBookEnabled) ||
                                (h.OnlyWeekendBookEnabled &&
                                (checkInDate.Value.IsWeekend() || checkOutDate.AddDays(-1).IsWeekend()) &&
                                !userReserves.Any(r => (r.CheckInDate.IsWeekend() || r.CheckOutDate.AddDays(-1).IsWeekend()) && r.HotelId == h.Id)
                            )
                      ))
                .ToList();

            var queryResult = hotels.Select(h => new HotelSearchResponseDTO
            {
                Id = h.Id,
                Name = h.Name,
                Stars = h.Stars,
                Location = h.Location,
                ChildPolicyDescription = h.ChildPolicyDescription,
                Description = h.Description,
                RoomType = h.RoomType,
                MealType = h.MealType,
                Images = h.Images.Select(img => $"{Request.Scheme}://{Request.Host.Host}:{Request.Host.Port}/api/Image/{img.Id}/View").ToList()
            }).ToList();

            var result = new HashSet<HotelSearchResponseDTO>();

            foreach (var resultItem in queryResult)
            {
                if (!userReservedHotelsIds.Contains(resultItem.Id))
                {
                    result.Add(resultItem);
                    continue;
                }

                if (checkInDate.Value.IsWeekend() || checkOutDate.IsWeekend())
                {
                    if (reservedHotelContDic[resultItem.Id] <= 4)
                        result.Add(resultItem);
                }
                else
                {
                    if (reservedHotelContDic[resultItem.Id] <= 6)
                        result.Add(resultItem);
                }
            }
            return result;
        }

        [AllowAnonymous]
        [HttpGet("ImportData")]
        public void ImportDataFromExcel()
        {
            var hotels = new List<Hotel>();
            Hotel hotelChain = null;
            var dbHotels = db.Hotels.ToHashSet();

            var excelEngine = new ExcelEngine();
            var application = excelEngine.Excel;
            application.DefaultVersion = ExcelVersion.Excel2013;
            var basePath = $"Data/Hotels.xlsx";
            var dataFile = new FileStream(basePath, FileMode.Open);
            var workbook = application.Workbooks.Open(dataFile);

            var sheet = workbook.Worksheets[0];

            for (var row = 2; row <= 51; row++)
            {
                try
                {
                    var hotel = new Hotel
                    {
                        Name = sheet.Range[$"B{row}"].Value.Trim(),
                        Location = sheet.Range[$"C{row}"].Value.Trim(),
                        IsActive = sheet.Range[$"D{row}"].Value.Trim() == "active",
                        HotelPlan = sheet.Range[$"E{row}"].Value.Trim() == "Dream Stay" ? Plan.DreamStay : sheet.Range[$"E{row}"].Value.Trim() == "platinum" ? Plan.Platinum : Plan.None,
                        MealType = sheet.Range[$"F{row}"].Value.Trim(),
                        RoomType = sheet.Range[$"G{row}"].Value.Trim(),
                        SingleBookLimitPerUser = sheet.Range[$"K{row}"].Value.Trim() == "once only",
                        AvailableDateIntervals = new HashSet<DateInterval>() {
                            new DateInterval
                            {
                                StartDate = DateTime.Today,
                                EndDate = DateTime.Today.AddMonths(3)
                            }
                        },
                        OnlyWeekendBookEnabled = sheet.Range[$"M{row}"].Value.Trim() == "Y",
                        ChildrenCountLimit = sheet.Range[$"N{row}"].Value.Trim() == "0" ? 0 : sheet.Range[$"N{row}"].Value.Trim() == "1" ? 1 : sheet.Range[$"N{row}"].Value.Trim() == "2" ? 2 : default,
                        ChildPolicyDescription = sheet.Range[$"O{row}"].Value.Trim(),
                        Stars = sheet.Range[$"Q{row}"].Value.Trim() == "5" ? 5 : sheet.Range[$"Q{row}"].Value.Trim() == "4" ? 4 : sheet.Range[$"Q{row}"].Value.Trim() == "3" ? 3 : sheet.Range[$"Q{row}"].Value.Trim() == "2" ? 2 : sheet.Range[$"Q{row}"].Value.Trim() == "1" ? 1 : 0,
                        Email = sheet.Range[$"R{row}"].Value.Trim(),
                        Description = sheet.Range[$"S{row}"].Value.Trim(),
                    };
                    if (sheet.Range[$"L{row}"].Value.Trim() == "A")
                    {
                        hotel.SingleBookLimitPerUserByHotelChain = true;
                        if (hotelChain == null)
                            hotelChain = hotel;
                        else
                            hotel.HotelChain = hotelChain;
                    }

                    if (dbHotels.Any(h => h.Name == hotel.Name)) // update
                    {
                        var dbHotel = db.Hotels.Include(h => h.Images).FirstOrDefault(h => h.Name == hotel.Name);
                        dbHotel.ChildPolicyDescription = hotel.ChildPolicyDescription;
                        dbHotel.Location = hotel.Location;
                        dbHotel.IsActive = hotel.IsActive;
                        dbHotel.HotelPlan = hotel.HotelPlan;
                        dbHotel.MealType = hotel.MealType;
                        dbHotel.RoomType = hotel.RoomType;
                        dbHotel.SingleBookLimitPerUser = hotel.SingleBookLimitPerUser;
                        dbHotel.OnlyWeekendBookEnabled = hotel.OnlyWeekendBookEnabled;
                        dbHotel.ChildrenCountLimit = hotel.ChildrenCountLimit;
                        dbHotel.ChildPolicyDescription = hotel.ChildPolicyDescription;
                        dbHotel.Stars = hotel.Stars;
                        dbHotel.Email = hotel.Email;
                        dbHotel.Description = hotel.Description;
                        dbHotel.HotelChain = hotel.HotelChain;
                        dbHotel.SingleBookLimitPerUserByHotelChain = hotel.SingleBookLimitPerUserByHotelChain;
                        if (!dbHotel.Images.Any(img => img.FileName == $"{dbHotel.Name}.jpg"))
                            dbHotel.Images.Add(new HotelImage
                            {
                                FileName = $"{hotel.Name}.jpg"
                            });
                    }
                    else // add
                    {
                        hotel.Images.Add(new HotelImage
                        {
                            FileName = $"{hotel.Name}.jpg"
                        });
                        hotels.Add(hotel);
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

            }
            try
            {
                db.Hotels.AddRange(hotels);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}
