﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class Role : IdentityRole<int>
    {
        public Role()
        {
        }
        public Role(string roleName) : base(roleName)
        {
        }
    }
}
