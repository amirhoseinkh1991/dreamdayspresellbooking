﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public bool IsActive { get; set; }
        public int Stars { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }
        public string Email { get; set; }
        public int? HotelChainId { get; set; }
        public Hotel HotelChain { get; set; }
        public string RoomType { get; set; }
        public string MealType { get; set; }
        public ICollection<HotelImage> Images { get; set; } = new HashSet<HotelImage>();
        public Plan HotelPlan { get; set; }
        public bool SingleBookLimitPerUser { get; set; }
        public bool SingleBookLimitPerUserByHotelChain { get; set; }
        public bool OnlyWeekendBookEnabled { get; set; }
        public int? ChildrenCountLimit { get; set; }
        public string ChildPolicyDescription { get; set; }
        public ICollection<DateInterval> AvailableDateIntervals { get; set; } = new HashSet<DateInterval>();
        public ICollection<StopSale> StopSales { get; set; } = new HashSet<StopSale>();
    }


}
