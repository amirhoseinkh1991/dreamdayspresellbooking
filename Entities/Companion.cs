﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class Companion
    {
        public int Id { get; set; }
        public CompanionType CompanionType { get; set; }
        public Gender Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? ReservationId { get; set; }
        public Reservation Reservation { get; set; }
    }

    public enum CompanionType
    {
        Adult,
        Child
    }
}
