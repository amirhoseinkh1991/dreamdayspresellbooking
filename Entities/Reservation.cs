﻿using PreSellBookingWebApi.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class Reservation
    {
        public int Id { get; set; }
        [Column(TypeName = "Date")]
        public DateTime BookingDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime CheckInDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime CheckOutDate { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int HotelId { get; set; }
        public Hotel Hotel { get; set; }
        public ICollection<Companion> Companions { get; set; } = new HashSet<Companion>();
        [NotMapped]
        public bool IsWeekend => CheckInDate.IsWeekend() || CheckOutDate.AddDays(-1).IsWeekend();
    }
}
