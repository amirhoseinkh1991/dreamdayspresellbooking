﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class UserDefaultCompanion
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CompanionId { get; set; }
        public User User { get; set; }
        public Companion Companion { get; set; }
    }
}
