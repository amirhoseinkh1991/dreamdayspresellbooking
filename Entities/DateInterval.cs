﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class DateInterval
    {
        public int Id { get; set; }
        [Column(TypeName = "Date")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime EndDate { get; set; }
        public int? HotelId { get; set; }
        public Hotel Hotel { get; set; }

    }
}
