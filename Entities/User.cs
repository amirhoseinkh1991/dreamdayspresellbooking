﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.Entities
{
    public class User : Microsoft.AspNetCore.Identity.IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; } = Gender.Unknown;
        public DateTime ExpirationDate { get; set; }
        public Plan UserPlan { get; set; }
        public ICollection<UserDefaultCompanion> DefaultCompanions { set; get; } = new HashSet<UserDefaultCompanion>();
    }

    public enum Gender
    {
        Male,
        Female,
        Unknown
    }
}
