﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.DTO
{
    public class UserConfigDTO
    {
        public UserDTO Leader { get; set; }
        public int TotalReservesRemaining { get; set; }
        public int TotalWeekendReservesRemaining { get; set; }
        public int ChildrenCompanionsCount { get; set; }
        public ICollection<CompanionDTO> Companions { get; set; }
        
    }
}
