﻿using PreSellBookingWebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.DTO
{
    public class HotelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stars { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string RoomType { get; set; }
        public string MealType { get; set; }
        public Plan HotelPlan { get; set; }
    }
}
