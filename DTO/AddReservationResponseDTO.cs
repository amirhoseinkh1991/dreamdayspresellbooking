﻿using PreSellBookingWebApi.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.DTO
{
    public class AddReservationResponseDTO
    {
        public int Id { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public UserDTO User { get; set; }
        public HotelDTO Hotel { get; set; }
        public ICollection<CompanionDTO> Companions { get; set; }
        public int ChildrenCount { get; set; }
        public bool IsWeekend => CheckInDate.IsWeekend() || CheckOutDate.IsWeekend();
    }
}
