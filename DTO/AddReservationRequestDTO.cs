﻿using PreSellBookingWebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.DTO
{
    public class AddReservationRequestDTO
    {
        public DateTime CheckInDate { get; set; }
        public int HotelId { get; set; }
    }
}
