﻿using PreSellBookingWebApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.DTO
{
    public class LoginResponseDTO
    {
        public string VoucherCode { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string LeaderFirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Token { get; set; }
    }
}
