﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PreSellBookingWebApi.DTO
{
    public class HotelSearchResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stars { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string RoomType { get; set; }
        public string MealType { get; set; }
        public string ChildPolicyDescription { get; set; }
        public ICollection<string> Images { get; set; }
    }
}
