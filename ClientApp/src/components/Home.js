import React, { useState } from 'react';
import axios from 'axios';
import { useCookies } from 'react-cookie';
import { useForm } from 'react-hook-form';
import logo from '../assets/LOGO.png';

const headers = {
	'Content-Type': 'application/json'
};

export default function Home() {
	const { register, handleSubmit } = useForm();
	const [ error, setError ] = useState(false);
	const [ cookies, setCookie ] = useCookies([ 'authorization' ]);
	const Login = (data) => {
		axios
			.post('http://46.101.247.243:5010/api/login', JSON.stringify(data), {
				headers: headers
			})
			.then((res) => {
				console.log(res);
				setCookie('authorization', `bearer ${res.data.Token}`, { path: '/' });
				if (res.data.LeaderFirstName === null) {
					window.location.replace('./fetch-data');
				} else {
					window.location.replace('./trip');
				}
			})
			.catch((err) => {
				setError(true);
			});
	};

	// setCookie('authorization', `bearer h8er7y89hg8w9e78b98n89`, { path: '/' });

	return (
		<div className="bg-image">
			<div>
				<img src={logo} width='15%' className="p-4 pl-4 ml-4"/>
			</div>
			<div className="row justify-content-md-center pl-5 pr-5">
				<form className="col-md-4 login-form" onSubmit={handleSubmit(Login)}>
					<div style={{ textAlign: 'left', fontSize: 24 }} className="mt-2 mb-4">
						Book your next exclusive summer stay deal here{' '}
					</div>
					<div className="form-group">
						{/* <label>Voucher code</label> */}
						<input
							type="text"
							className="form-control"
							placeholder="voucher code"
							name="userName"
							ref={register}
						/>
						{/* <small id="emailHelp" className="form-text text-muted">
							Please enter the vocher code to continue.
						</small> */}
					</div>
					<div className="form-group">
						{/* <label>Password</label> */}
						<input
							type="password"
							className="form-control"
							placeholder="password"
							name="password"
							ref={register}
						/>
					</div>
					{error ? (
						<div style={{ color: 'red', textAlign: 'center' }}>user name or password is invalid.</div>
					) : null}
					<div className="row justify-content-sm-center">
						<button
							type="submit"
							className="btn btn-primary"
							style={{ paddingLeft: 32, paddingRight: 32, backgroundColor: 'red', border: 0 }}
						>
							Login
						</button>
					</div>
				</form>
			</div>
			<div className="tx-c show-text">
				Stay 2 nights every week in one of the luxury hotels and reports in UAE for 6 weeks valid for 3 month.
			</div>
		</div>
	);
}
