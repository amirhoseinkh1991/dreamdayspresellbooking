import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useCookies } from 'react-cookie';
import logo from '../assets/LOGO.png';
export default function Confirmation() {
	const [ data, setData ] = useState({});
	const [ cookies, setCookie ] = useCookies([ 'authorization' ]);
	const [ loaded, setLoading ] = useState(false);
	useEffect(() => {
		axios
			.get('http://46.101.247.243:5010/api/Reservation/List/ByCurrentUser', {
				headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
			})
			.then((res) => {
				setData(res.data[res.data.length - 1]);
				setLoading(true);
			})
			.catch((err) => {});
	}, []);
	return loaded ? (
		<div>
			{console.log(data)}
			<div className="container bg-white data-form mt-3 p-5">
				<div className="row pl-5 pr-5">
					<div className="container tx-c mb-4">
						<img src={logo} width="15%" />
					</div>
					<div className="row col-sm-12">
						<div className="tx-c mt-4 col-sm-12" style={{ fontSize: 24, color: 'red' }}>
							Processing Your Confirmation
						</div>
					</div>
					<div className="row col-sm-12 pl-5 pr-5">
						<div className="row mt-4 pl-5 pr-5">
							<div className="mt-2 mb-4 col-sm-12">
								Dear {data.User.FirstName} {data.User.LastName},
							</div>
							<div className="mt-2 mb-4 col-sm-12">
								Your booking is in process and we will send you your confirmation shortly
							</div>
							{/* <div className="mt-2 mb-4 col-sm-12">
								2 nights ${data.IsWeekend ? 'weekend' : ''}
							</div> */}
							<div className="mt-2 mb-4 col-sm-12">Your booking details</div>
							<div className="container">
								<div className="mt-1 mb-3 col-sm-12 grey">hotel name : {data.Hotel.Name}</div>
								<div className="mt-1 mb-3 col-sm-12 grey">reserve date : {data.BookingDate}</div>
								<div className="mt-1 mb-3 col-sm-12 grey">check-in date : {data.CheckInDate}</div>
								<div className="mt-1 mb-3 col-sm-12 grey">check-out date : {data.CheckOutDate}</div>
								<div className="mt-1 mb-3 col-sm-12 grey">roome type : {data.Hotel.RoomType}</div>
								<div className="mt-1 mb-3 col-sm-12 grey">meal type : {data.Hotel.MealType}</div>
							</div>
							<div className="mt-2 mb-4 col-sm-12">Booking names are : </div>
							<div className="container">
								<div className="mt-1 mb-3 col-sm-12 grey">
									1 - {data.User.FirstName} {data.User.LastName}
								</div>
								{data.Companions.map((comp, index) => {
									return (
										<div className="mt-1 mb-3 col-sm-12 grey">
											{index + 2} - {comp.FirstName} {comp.LastName}
										</div>
									);
								})}
							</div>
							<div className="mt-2 mb-4 col-sm-12">
								Your book is nonrefundable and cannot br cancel after you recieve the confirmation.You
								can extend or upgrade your room at the hotel ith 20% discount rate, subject to
								availability.if you wish to extend or any arangemanet feel free to contact support team
								at <b>800 2080</b>
							</div>
						</div>
						<div className="row col-sm-12">
							<div className="mt-5 tx-c col-sm-12">
								<button
									type="button"
									onClick={() => {
										window.location.replace('./trip');
									}}
									style={{
										backgroundColor: 'red',
										border: 0,
										color: 'white',
										borderRadius: 4,
										padding: '8px 24px'
									}}
								>
									Book yout next trip
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	) : null;
}
