import React, { useState, useEffect } from 'react';
import Hotel from './Hotel';
import Calendar from 'react-calendar';
import axios from 'axios';
import { useCookies } from 'react-cookie';
import moment from 'moment';
import logo from '../assets/LOGO.png';
import { useClickOutside } from 'react-click-outside-hook';

export default function Trip() {
	const [ date, setDate ] = useState(moment().add(1, 'd')._d);
	const [ result, setResult ] = useState([]);
	const [ cookies, setCookie ] = useCookies([ 'authorization' ]);
	const [ loading, setLoading ] = useState(false);
	const [ config, setConfig ] = useState({});
	const [ reserveList, setList ] = useState([]);
	const [ disabledDates, setDates ] = useState([]);
	const [ loadCalendar, setLoaded ] = useState(false);
	const [ ref, hasClickedOutside ] = useClickOutside();

	useEffect(() => {
		fetch('http://46.101.247.243:5010/api/User/Current/Config', {
			method: 'get', // *GET, POST, PUT, DELETE, etc.
			credentials: 'same-origin', // include, *same-origin, omit
			headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
		})
			.then((response) => (response.status === 401 ? window.location.replace('/') : response.json()))
			.then((data) => setConfig(data))
			.catch((error) => {
				console.log('==========Error===========:', error);
			});

		// axios
		// 	.get('http://46.101.247.243:5010/api/User/Current/Config', {
		// 		headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
		// 	})
		// 	.then((res) => {
		// 		setConfig(res.data);
		// 		console.log('============================', res);
		// 	})
		// 	.catch((err) => {});
		axios
			.get('http://46.101.247.243:5010/api/Reservation/List/ByCurrentUser', {
				headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
			})
			.then((res) => {
				setList(res.data);
				let temp = [];
				temp.push(moment()._d);
				for (let i = 0; i < res.data.length; i++) {
					temp.push(moment(res.data[i].CheckInDate)._d);
					temp.push(moment(res.data[i].CheckInDate).add(1, 'd')._d);
					temp.push(moment(res.data[i].CheckInDate).add(-1, 'd')._d);
				}
				setDates(temp);
			})
			.catch((err) => {});
	}, []);

	const openCalendar = function() {
		setLoaded(true);
	};

	const setCalendar = function(value) {
		setDate(value);
		setLoaded(false);
	};
	const search = function() {
		setLoading(true);
		axios
			.get(
				`http://46.101.247.243:5010/api/Hotels/Search?checkInDate=${moment(date).format(
					'YYYY-MM-DD'
				)}&children=${2}`,
				{
					headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
				}
			)
			.then((res) => {
				setResult(res.data);
				setLoading(false);
			});
	};
	return (
		<div className="container bg-white data-form mt-3 p-5" style={{ height: '100vh', overflowY: 'scroll' }}>
			<div className="container tx-c mb-4">
				<img src={logo} width="15%" />
			</div>
			<div className="row">
				<div className="col-sm tx-c mt-4" style={{ fontSize: 24 }}>
					Choose your date and hotel
				</div>
			</div>

			<div className="container mt-3 mb-4">
				<div className="row mt-2">
					{'Leader' in config ? <div className="col-sm tx-l">Plan : {config.Leader.Plan}</div> : null}
				</div>
				<div className="row mt-2">
					{'Leader' in config ? (
						<div className="col-sm tx-l">
							Primary info : {config.Leader.FirstName} {config.Leader.LastName}
						</div>
					) : null}
				</div>
				<div className="row mt-2">
					{'Leader' in config ? (
						<div className="col-sm tx-l">
							Companion info :{' '}
							{config.Companions.map((comp, index) => {
								return (
									<span style={{ marginRight: 8 }}>
										{index + 1}- {comp.FirstName} {comp.LastName} ({comp.Gender})
									</span>
								);
							})}
						</div>
					) : null}
				</div>
			</div>
			<div className="row mt-2 mb-2">
				<div className="col-sm tx-c trip-cont">
					Total of <b>{config.TotalReservesRemaining}</b> trips left, out of that{' '}
					<b>{config.TotalWeekendReservesRemaining}</b> "trip can be in weekend and holidays", subject to
					availability{' '}
				</div>
			</div>
			{/* <div className="row mt-3">
				<div className="col-sm tx-l mt-3">
					Select your Plan : <button className="select-button mr-2">First trip + date</button>{' '}
					<button className="select-button active-select ">Seconde trip</button>
				</div>
			</div> */}
			<div className="container">
				<div className="col-sm tx-l mt-3">
					<div className="row mt-5">
						<div className="col-sm-2 mt-2">Choose your date</div>
						{/* {console.log(
							moment(date).date(),
							disabledDates,
							disabledDates.indexOf(moment(date).hour(0).minute(0).second(0)._d)
						)} */}
						<input
							className="form-control col-sm-3"
							onFocus={openCalendar}
							value={`${moment(date).format('DD/MM/YYYY')} ~ ${moment(date)
								.add(2, 'd')
								.format('DD/MM/YYYY')}`}
						/>
						<div className=" col-sm-2" style={{ marginLeft: 12, marginTop: 6 }}>
							{moment(date).day() === 4 || moment(date).day() === 5 ? (
								'Weekend booking'
							) : (
								'Weekdays booking'
							)}
						</div>
						<div className="edit-button col-sm-4">
							<button
								className=" btn"
								type="button"
								onClick={() => window.location.replace('/fetch-data')}
							>
								edit companion
							</button>
						</div>
					</div>
					{loadCalendar ? (
						// style={{ display: hasClickedOutside ? 'none' : 'block' }}
						<div ref={ref}>
							<Calendar
								className="mt-3 calendar"
								calendarType="Arabic"
								value={date}
								onChange={(value) => setCalendar(value)}
								minDate={new Date()}
								tileDisabled={({ date, view }) =>
									disabledDates.some(
										(disabledDate) =>
											date.getFullYear() === disabledDate.getFullYear() &&
											date.getMonth() === disabledDate.getMonth() &&
											date.getDate() === disabledDate.getDate()
									)}
							/>
						</div>
					) : null}
				</div>
			</div>

			<div className="mt-5 tx-c">
				<button
					type="button"
					onClick={search}
					style={{
						backgroundColor: 'green',
						border: 0,
						color: 'white',
						borderRadius: 4,
						padding: '8px 24px'
					}}
				>
					Search
				</button>
			</div>

			{loading ? (
				<div className="spinner-border text-primary spinner-cont" role="status">
					<span className="sr-only">Loading...</span>
				</div>
			) : (
				<div className="container mt-4">
					{result.map.length === 0 ? (
						<div>no hotel found</div>
					) : (
						<div className="container">
							{result.length > 0 ? <div className="mt-2 mb-5 ">Choose your hotel</div> : null}
							{result.map((hotel, index) => {
								return (
									<div key={index} className="mb-4">
										<Hotel hotel={hotel} config={config} date={date} />
									</div>
								);
							})}
						</div>
					)}
				</div>
			)}
		</div>
	);
}
