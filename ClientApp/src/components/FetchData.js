import React, { useState, useEffect } from 'react';
import Companion from './companion';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { useCookies } from 'react-cookie';
import logo from '../assets/LOGO.png';
import Toast from 'react-bootstrap/Toast';
import { Modal, Button } from 'react-bootstrap';
const rawCompanion = { gender: 'male', CompanionType: 'Child', FirstName: '', LastName: '' };

function WarningModal(props) {
	return (
		<Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered>
			<Modal.Header closeButton>
				<Modal.Title id="contained-modal-title-vcenter">Warning</Modal.Title>
			</Modal.Header>
			<Modal.Body style={{ padding: '1rem', textAlign: 'center' }}>
				<h6 className="mt-5 mb-5 pl-2 pr-2" style={{ textAlign: 'justify' }}>
					Your are adding more than 2 companion and it might reuduce your choices in hotel results and
					booking. do you want to proceed ?
				</h6>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="light" onClick={props.onHide}>
					No
				</Button>
				<Button variant="success" onClick={props.heDontCare}>
					Yes, Next
				</Button>
			</Modal.Footer>
		</Modal>
	);
}

export default function Register() {
	const { register, handleSubmit } = useForm();
	const [ defaultLeader, setDefaultLeader ] = useState({});
	const [ leader, setLeader ] = useState({});
	const [ companion, setCompanion ] = useState([]);
	const [ error, setError ] = useState(false);
	const [ cookies ] = useCookies([ 'authorization' ]);
	const [ data, setData ] = useState({});
	const [ loaded, setLoad ] = useState(true);
	const [ editMode, setMode ] = useState(true);
	const [ showToast, setToast ] = useState(false);
	const [ errorToast, setErrorToast ] = useState(false);
	const [ confirm, setConfirm ] = useState(false);
	const [ modalShow, setModalShow ] = React.useState(false);

	const onSubmit = (data) => {
		setLeader(data);
		console.log(data);
	};
	useEffect(() => {
		fetch('http://46.101.247.243:5010/api/User/Current/Config', {
			method: 'get', // *GET, POST, PUT, DELETE, etc.
			credentials: 'same-origin', // include, *same-origin, omit
			headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
		})
			.then((response) => (response.status === 401 ? window.location.replace('/') : response.json()))
			.then((data) => {
				setData(data);
				setLeader(data.Leader);
				setDefaultLeader(data.Leader);
				setCompanion(data.Companions);
				setLoad(true);
				if (
					data.Leader.FirstName === '' ||
					data.Leader.FirstName === null ||
					data.Leader.FirstName === 'null'
				) {
					setMode(false);
				}
			})
			.catch((error) => {
				console.log('==========Error===========:', error);
			});
		axios
			.get('http://46.101.247.243:5010/api/User/Current/Config', {
				headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
			})
			.then((res) => {})
			.catch((err) => {});
	}, []);
	const closeToast = () => setToast(!showToast);
	const closeErrorToast = () => setErrorToast(!errorToast);
	const addCompanion = () => {
		if (companion.length < 3) {
			setCompanion([ ...companion, rawCompanion ]);
		} else {
			setError(true);
		}
	};
	const heDontCare = () => {
		setConfirm(true);
		nextStep();
	};
	const SetDataHandler = (data, index) => {
		let temp = companion;
		temp[index] = data;
		setCompanion([ ...temp ]);
	};
	const removeCompanion = (index) => {
		let temp = companion;
		temp.splice(index, 1);
		setCompanion([ ...temp ]);
	};
	const nextStep = () => {
		let child = 0;
		if (companion.length > 2 && !confirm) {
			setModalShow(true);
			return;
		}
		for (let i = 0; i < companion.length; i++) {
			if (companion[i].FirstName === '' || companion[i].LastName === '') {
				setErrorToast(true);
				return;
			}
			if (companion[i].CompanionType === 'Child' || companion[i].companionType === 'Child') {
				child++;
			}
		}

		if (
			leader.FirstName === '' ||
			leader.LastName === '' ||
			leader.email === '' ||
			leader.phoneNumber === '' ||
			leader.firstName === '' ||
			leader.lastName === '' ||
			leader.Email === '' ||
			leader.PhoneNumber === ''
		) {
			setErrorToast(true);
			return;
		}
		if (child > 2) {
			setToast(true);
		} else {
			let data = {
				firstName: leader.firstName,
				lastName: leader.lastName,
				gender: leader.gender,
				email: leader.email,
				phoneNumber: leader.phoneNumber,
				defaultCompanions: companion
			};
			axios
				.post('http://46.101.247.243:5010/api/User/Current/EditDetails', data, {
					headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' }
				})
				.then((res) => {
					window.location.replace('./trip');
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	return loaded && Object.keys(leader).length !== 0 ? (
		<div className="row mt-2 ">
			<Toast
				show={showToast}
				onClose={closeToast}
				autohide
				style={{ position: 'absolute', zIndex: 10, right: 24 }}
			>
				<Toast.Header>
					<strong className="mr-auto">Warning</strong>
				</Toast.Header>
				<Toast.Body>Sorry you cannot add more than 2 childs</Toast.Body>
			</Toast>
			<Toast
				show={errorToast}
				onClose={closeErrorToast}
				autohide
				style={{ position: 'absolute', zIndex: 10, right: 24 }}
			>
				<Toast.Header>
					<strong className="mr-auto">Warning</strong>
				</Toast.Header>
				<Toast.Body>Please fill out the form</Toast.Body>
			</Toast>
			<div className="col-sm-2" />
			<div
				className="col-sm-8 data-form bg-white"
				style={{ padding: '4rem', overflowY: 'scroll', height: '90vh' }}
			>
				<div>
					<div className="container tx-c mb-4">
						<img src={logo} width="20%" />
					</div>
					<div className="row">
						<div className="col-sm tx-c" style={{ fontSize: 24 }}>
							submit your information
						</div>
					</div>
					<div className="row mt-4">
						<div className="col-sm-4 tx-l mt-3">voucher Code : {defaultLeader.VoucherCode} </div>
						<div className="col-sm-4 tx-l mt-3">expire date : {defaultLeader.ExpirationDate} </div>
						<div className="col-sm-4 tx-l mt-3">Plan : {defaultLeader.Plan} </div>
					</div>

					<div className="container">
						<form>
							<div className="row mt-5">
								<div className="col-sm-6 tx-l  mt-2 row" style={{ color: 'red' }}>
									Primary Guest Information
								</div>

								<div className="form-group col-sm-6 row">
									<label className="col-sm-4 mt-1">Gender</label>
									<select
										className="form-control col-sm-8"
										name="gender"
										value={leader.Gender}
										ref={register}
										onChange={handleSubmit(onSubmit)}
										disabled={editMode}
									>
										<option value="Male">male</option>
										<option value="Female">female</option>
									</select>
								</div>
							</div>
							<div className="row mt-2">
								<div className="row form-group col-sm-6">
									<label>first name</label>
									<input
										type="text"
										className="form-control"
										placeholder="First name"
										name="firstName"
										value={leader.FirstName}
										ref={register}
										onChange={handleSubmit(onSubmit)}
										disabled={editMode}
									/>
								</div>
								<div className="form-group col-sm-6">
									<label>last name</label>
									<input
										type="text"
										className="form-control"
										placeholder="Last name"
										name="lastName"
										value={leader.LastName}
										ref={register}
										onChange={handleSubmit(onSubmit)}
										disabled={editMode}
									/>
								</div>
							</div>
							<div className="row mt-2">
								<div className="row form-group col-sm-6">
									<label>E-mail</label>
									<input
										type="email"
										className="form-control"
										name="email"
										aria-describedby="emailHelp"
										placeholder="email"
										value={leader.Email}
										ref={register}
										onChange={handleSubmit(onSubmit)}
									/>
								</div>
								<div className="form-group col-sm-6">
									<label>Phone number</label>
									<input
										type="number"
										name="phoneNumber"
										className="form-control"
										placeholder="Phone number"
										value={leader.PhoneNumber}
										ref={register}
										onChange={handleSubmit(onSubmit)}
										disabled={editMode}
									/>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div style={{ marginTop: '5%' }} className="row">
					<div className=" col-sm-4">
						<button
							type="button"
							className="btn"
							onClick={addCompanion}
							style={{ backgroundColor: 'red', color: 'white' }}
						>
							Add companion
						</button>
					</div>
					<div className="col-sm-8 mt-2" style={{ color: 'grey' }}>
						Note : your membership is not transferable to another person
					</div>
					<div className="row">
						<small className="col-sm tx-c mt-4 error-color">
							{' '}
							{error ? `sorry , you cannot add more than 3 person` : null}
						</small>
					</div>
				</div>

				<div className="row mt-5">
					{companion.map((data, index) => {
						return (
							<Companion
								key={index}
								className="col-sm-10"
								setData={SetDataHandler}
								removeCompanion={removeCompanion}
								index={index}
								companion={data}
							/>
						);
					})}
				</div>
				<div className="steps-button">
					<button type="button" onClick={nextStep}>
						Submit
					</button>
				</div>
			</div>
			<WarningModal show={modalShow} heDontCare={heDontCare} onHide={() => setModalShow(false)} />
		</div>
	) : null;
}
