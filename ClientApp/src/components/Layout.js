import React, { Component } from 'react';
import { Container } from 'reactstrap';

export class Layout extends Component {
	static displayName = Layout.name;

	render() {
		return (
			<div className="bg-image">
				{window.location.pathname === '/' ? null : (
					<div className="pt-4 pb-4 header">
						<ul className="vcv-timeline">
							<li
								className={`vcv-timeline-item ${window.location.pathname === '/trip' ||
								window.location.pathname === '/confirmation'
									? 'vcv-step-done'
									: null}`}
								data-step="1"
								data-step-title="Download"
							>
								<div style={{ width: '100%' }}>
									Submit your information<br />
									<small>Fill your information for your reserve</small>
								</div>
							</li>
							<li
								className={`vcv-timeline-item ${window.location.pathname === '/confirmation'
									? 'vcv-step-done'
									: null}`}
								data-step="2"
							>
								<div style={{ width: '100%' }}>
									Choose your hotel
									<br />
									<small>Choose your check-in date and hotel stay</small>
								</div>
							</li>
							<li
								className={`vcv-timeline-item ${window.location.pathname === '/confirmation'
									? 'vcv-step-done'
									: null}`}
								data-step="3"
							>
								<div style={{ width: '100%' }}>
									Confirmation
									<br />
									<small>Booking confirmation and details</small>
								</div>
							</li>
						</ul>
					</div>
				)}
				<div className="color-bg">{this.props.children}</div>
			</div>
		);
	}
}
