import React from 'react';
import { useForm } from 'react-hook-form';

export default function Companion(props) {
	const { register, handleSubmit } = useForm();
	const onSubmit = (data) => props.setData(data, props.index);
	return (
		<div className="container mt-4 pb-2 bb-1">
			{console.log('=====', props)}
			<form>
				<div>
					<div className="form-group row">
						<div className="col-sm-3 tx-l mt-2" style={{ color: 'red' }}>
							Companion Information
						</div>
						<div className="col-sm-3">
							<select
								className="form-control"
								name="companionType"
								value={props.index > 0 ? 'Child' : 'Adult'}
								ref={register}
								onChange={handleSubmit(onSubmit)}
								required
							>
								<option value="Adult" disabled={props.index > 0}>
									adult
								</option>
								<option value="Child">child</option>
								{/* <option value="u12">child under 12</option> */}
							</select>
						</div>
						<div className="form-group col-sm-6 row">
							<label className="col-sm-4 mt-1">Gender</label>
							<div className="form-group col-sm-8">
								<select
									className="form-control"
									name="gender"
									value={props.companion.Gender}
									ref={register}
									onChange={handleSubmit(onSubmit)}
									required
								>
									<option value="Male">male</option>
									<option value="Female">female</option>
								</select>
							</div>
						</div>
					</div>

					<div className="row">
						<div className="form-group col-sm-6">
							<label>first name</label>
							<input
								type="text"
								name="firstName"
								ref={register}
								value={props.companion.FirstName}
								className="form-control"
								placeholder="First name"
								onChange={handleSubmit(onSubmit)}
								required
							/>
						</div>
						<div className="form-group col-sm-5">
							<label>last name</label>
							<input
								type="text"
								name="lastName"
								ref={register}
								value={props.companion.LastName}
								className="form-control"
								placeholder="Last name"
								onChange={handleSubmit(onSubmit)}
								required
							/>
						</div>
						<div
							className="col-sm-1 dmt"
							style={{ color: 'red', cursor: 'pointer' }}
							onClick={() => props.removeCompanion(props.index)}
						>
							X
						</div>
					</div>
				</div>
			</form>
		</div>
	);
}
