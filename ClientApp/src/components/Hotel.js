import React from 'react';
import ReactStars from 'react-rating-stars-component';
import { Modal, Button } from 'react-bootstrap';
import moment from 'moment';
import axios from 'axios';
import { useCookies } from 'react-cookie';
function HotelModal(props) {
	console.log(props);
	const [ cookies, setCookie ] = useCookies([ 'authorization' ]);
	const book = function() {
		axios
			.post(
				'http://46.101.247.243:5010/api/Reservation/Add',
				{
					checkInDate: moment(props.date).add(1, 'd')._d,
					hotelId: props.hotel.Id
				},
				{ headers: { authorization: cookies.authorization, 'Content-Type': 'application/json' } }
			)
			.then((res) => {
				window.location.replace('./confirmation');
			});
	};
	return (
		<Modal {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered>
			<Modal.Header closeButton>
				<Modal.Title id="contained-modal-title-vcenter">Reservation Data</Modal.Title>
			</Modal.Header>
			<Modal.Body style={{ padding: '1rem', textAlign: 'center' }}>
				<h4>{props.hotel.Name}</h4>
				<p>
					<h6 className="mt-1">Check-in date :</h6>
					{moment(props.date).format('MM/DD')} <br />
					<h6 className="mt-1">Check-out date :</h6>
					{moment(props.date).add(2, 'd').format('MM/DD')}
					<h6 className="mt-1">Leader name :</h6>
					<div>
						{props.config.Leader.FirstName} {props.config.Leader.LastName}
					</div>
					{props.config.Companions.length > 0 ? <h6 className="mt-2">Companions : </h6> : null}
					{props.config.Companions.map((item, index) => {
						return (
							<div className="mt-2">
								{item.FirstName} {item.LastName} ({item.CompanionType}/{item.Gender})
							</div>
						);
					})}
				</p>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="light" onClick={props.onHide}>
					Close
				</Button>
				<Button variant="success" onClick={book}>
					Book
				</Button>
			</Modal.Footer>
		</Modal>
	);
}

export default function Hotel(data) {
	const [ modalShow, setModalShow ] = React.useState(false);
	return (
		<div className="row hotel-card">
			<div
				className="col-sm-3 hotel-image mr-4"
				style={{
					backgroundImage: `url(${data.hotel.Images[0]})`,
					backgroundPosition: 'center center',
					backgroundSize: 'cover',
					backgroundRepeat: 'no-repeat',
					minHeight: '180px'
				}}
			/>
			<div className="row col-sm-8" style={{ borderRadius: 2, border: '1px solid #a7a7a7' }}>
				<div className="row col-sm-10 pl-4 pt-3 pb-2">
					<div className="col-sm-12">hotel name : {data.hotel.Name}</div>
					{/* <div className="col-sm-12">
					{' '}
					<ReactStars value={data.hotel.Stars} size={24} color2={'#ffd700'} edit={false} />
				</div> */}

					<div className="col-sm-12">meal type : {data.hotel.MealType}</div>
					<div className="col-sm-12">room type : {data.hotel.RoomType}</div>
					{data.hotel.ChildPolicyDescription !== '' ? (
						<div className="col-sm-12">child policy : {data.hotel.ChildPolicyDescription}</div>
					) : null}
					{data.hotel.Description !== '' ? (
						<div className="col-sm-12">description : {data.hotel.Description} </div>
					) : null}
					{data.hotel.Location !== '' ? (
						<div className="col-sm-12">locatin : {data.hotel.Location} </div>
					) : null}
				</div>
				<div className="col-sm-2" style={{ postion: 'relative' }}>
					<button
						className="select-hotel"
						data-toggle="modal"
						data-target="#exampleModal"
						onClick={() => setModalShow(true)}
					>
						select
					</button>
				</div>
			</div>

			<HotelModal
				show={modalShow}
				hotel={data.hotel}
				config={data.config}
				date={data.date}
				onHide={() => setModalShow(false)}
			/>
		</div>
	);
}
