import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import Home from './components/Home';
import FetchData from './components/FetchData';
import Trip from './components/Trip';
import Confirmation from './components/Confirmation';
import 'react-calendar/dist/Calendar.css';
import './custom.css';
import 'antd/dist/antd.css';

export default class App extends Component {
	static displayName = App.name;

	render() {
		return (
			<Layout>
				<Route exact path="/" component={Home} />
				<Route path="/trip" component={Trip} />
				<Route path="/fetch-data" component={FetchData} />
				<Route path="/confirmation" component={Confirmation} />
			</Layout>
		);
	}
}
